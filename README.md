#   1일 최대 투여량 의약품 정보 APP


### LANGUAGE
```
- Flutter 3.3.2
- Dart 2.18.1
```


### 기능
```
* 의약품정보 
 - 성분코드에 따른 의약품 정보 확인
 - 상세검색을 통해 의약품 정보 확인
```

### 앱 화면
> * 메인화면

![app drug all](./images/app drug.all.png)
![app search](./images/app search.png)