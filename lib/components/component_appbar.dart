import 'package:flutter/material.dart';

class ComponentAppbar extends StatefulWidget implements PreferredSizeWidget{
  const ComponentAppbar(
  {super.key, required this.title,
  required this.actionIcon,
  required this.callback});

  final String title;
  final IconData actionIcon;
  final VoidCallback callback;


  @override
  State<ComponentAppbar> createState() => _ComponentAppbarState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(40);
  }
}


class _ComponentAppbarState extends State<ComponentAppbar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(widget.title),
      elevation: 1.0,
      actions: [
        IconButton(onPressed: widget.callback,
            icon: Icon(widget.actionIcon))
      ],
    );
  }
}
