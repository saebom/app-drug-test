import 'package:app_api_test_drug/model/drug_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
    const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

    final DrugListItem item;
    final VoidCallback callback;

    @override
    Widget build(BuildContext context) {
      return GestureDetector(
        onTap: callback,
        child: Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: const Color.fromRGBO(100, 100, 100, 100)),
          ),
          child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              '성분코드:${item.CPNT_CD == null ? '성분코드가 없습니다.' : item.CPNT_CD}',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              '성분명:${item.DRUG_CPNT_KOR_NM == null ? '성분명이 없습니다.' : item.DRUG_CPNT_KOR_NM}',
              style: TextStyle(fontSize: 13),
            ),
            Text(
              '제형명:${item.FOML_NM == null ? '제형명이 없습니다.' : item.FOML_NM}',
              style: TextStyle(fontSize: 13),
            ),
            Text(
              '투여경로:${item.DOSAGE_ROUTE_CODE == null ? '투여경로가 없습니다.' : item.DOSAGE_ROUTE_CODE}',
              style: TextStyle(fontSize: 13),
            ),
            Text(
              '1일최대투여량:${item.DAY_MAX_DOSG_QY == null ? '1일최대투여량이 없습니다.' : item.DAY_MAX_DOSG_QY}',
              style: TextStyle(fontSize: 13),
            ),
          ],
          ),
        ),
      );
    }
}
